#include <pybind11/pybind11.h>
#include <pybind11/stl.h>

#include <array>
#include <iostream>
#include <memory>
#include <vector>

namespace py = pybind11;

#include "flxcard/FlxCard.h"

struct flxcard_py {
    enum LOCK {
        NONE = LOCK_NONE,
        DMA0 = LOCK_DMA0,
        DMA1 = LOCK_DMA1,
        I2C = LOCK_I2C,
        FLASH = LOCK_FLASH,
        ELINK = LOCK_ELINK,
        ALL = LOCK_ALL
    };
};

PYBIND11_MODULE(libflxcard_py, m) {
    m.doc() = "Python bindings for FlxCard.";

    py::class_<FlxCard, std::shared_ptr<FlxCard>> flx(m, "flxcard");

    flx.def(py::init())

        // Interaction with the driver
        // card_open(int n, u_int lock_mask, bool read_config=false, bool ignore_version=false)
        .def("card_open", &FlxCard::card_open, py::arg("n"), py::arg("lock_mask"), py::arg("read_config")=false, py::arg("ignore_version")=false)
        // void card_close(void);
        .def("card_close", &FlxCard::card_close)
        // static u_int number_of_cards(void);
        .def_static("number_of_cards", &FlxCard::number_of_cards)
        // static u_int number_of_devices(void);
        .def_static("number_of_devices", &FlxCard::number_of_devices)
        // static int card_to_device_number( int card_number );
        .def_static("card_to_device_number", &FlxCard::card_to_device_number, py::arg("card_number"))
        // u_int get_lock_mask(int n);
        .def("get_lock_mask", &FlxCard::get_lock_mask, py::arg("n"))

        // Register and bitfield access
        // u_long cfg_get_option(const char *key, bool show_options=false);
        .def("cfg_get_option", &FlxCard::cfg_get_option, py::arg("key"), py::arg("show_options")=false)
        // u_long cfg_get_reg(const char *key);
        .def("cfg_get_reg", &FlxCard::cfg_get_reg, py::arg("key"))
        // void cfg_set_option(const char *key, u_long value, bool show_options=false);
        .def("cfg_set_option", &FlxCard::cfg_set_option, py::arg("key"), py::arg("value"), py::arg("show_options")=false)
        // void cfg_set_reg(const char *key, u_long value);
        .def("cfg_set_reg", &FlxCard::cfg_set_reg, py::arg("key"), py::arg("value"))

        // i2c
        .def("read_i2c", [](FlxCard& flxcard, const std::string& device_str, u_char reg_addr, int nbytes) {
            // void i2c_read( const char *device_str, u_char reg_addr, u_char *value, int nbytes = 1 );
            u_char value[256];
            flxcard.i2c_read(device_str.c_str(), reg_addr, value, nbytes);

            std::vector<u_char> r;
            for (int i=0; i<nbytes; i++) {
                r.push_back(value[i]);
            }
            return r;
        })
        .def("read_i2c", [](FlxCard& flxcard, const std::string& device_str, u_char reg_addr) {
            // void i2c_read( const char *device_str, u_char reg_addr, u_char *value, int nbytes = 1 );
            u_char value[1];
            flxcard.i2c_read(device_str.c_str(), reg_addr, value, 1);
            return value[0];
        })
        .def("write_i2c", [](FlxCard &flxcard, const std::string& device_str, u_char reg_addr, u_char value) {
            // void i2c_write( const char *device_str, u_char reg_addr, u_char data );
            flxcard.i2c_write(device_str.c_str(), reg_addr, value);
        })
        .def("write_i2c", [](FlxCard &flxcard, const std::string& device_str, u_char reg_addr, std::vector<u_char> data) {
            // void i2c_write( const char *device_str, u_char reg_addr, u_char *data, int nbytes );
            u_char value[256];
            int nbytes = data.size();
            for (int i=0; i<nbytes; i++) {
                value[i] = data[i];
            }
            flxcard.i2c_write(device_str.c_str(), reg_addr, value, nbytes);
        })

        // Tools
        // int card_model(void);
        .def("card_model", &FlxCard::card_model)
        // void soft_reset(void);
        .def("soft_reset", &FlxCard::soft_reset)
        // void registers_reset(void);
        .def("registers_reset", &FlxCard::registers_reset)
        // monitoring_data_t get_monitoring_data(u_int device_mask);
        // u_int number_of_channels(void);
        .def("number_of_channels", &FlxCard::number_of_channels)

        // Service functions
        // u_long openBackDoor(int bar);
        .def("openBackDoor", &FlxCard::openBackDoor, py::arg("bar"))
    ;

    py::enum_<flxcard_py::LOCK>(flx, "LOCK", py::arithmetic())
        .value("NONE", flxcard_py::LOCK::NONE)
        .value("DMA0", flxcard_py::LOCK::DMA0)
        .value("DMA1", flxcard_py::LOCK::DMA1)
        .value("I2C", flxcard_py::LOCK::I2C)
        .value("FLASH", flxcard_py::LOCK::FLASH)
        .value("ELINK", flxcard_py::LOCK::ELINK)
        .value("ALL", flxcard_py::LOCK::ALL)
        .export_values();
}
